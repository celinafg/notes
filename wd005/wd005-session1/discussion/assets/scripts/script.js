// let div = document.getElementById('mainDiv');
// get ELEMENT only because IDs are unique 
// document.getElementsbyTagName('li').style.color="red";
// console.log(li)


// document.getElementsByTagName('li')[2].style.color="red";
// document.getElementsByClassName('fruit');

//query selector will need #sign unlike getElement byId
//will choose first element 
// let =document.querySelector('.fruit')
// console.log(div)
//will choose all 
// let div =document.querySelectorAll('.fruit')
// let fruit = document.getElementsByClassName('fruit')
// console.log(fruit)

/* selectors: 
1. document.getElementById()
2. document.getElementsByTagName()
3. document.getElementsByClassName()
4. document.querySelectorAll()*/

// let div=document.querySelectorAll('.fruit')

// div.forEach(function(indivFruit){
// 	indivFruit.style.color="red";
// })

// let div = document.getElementById('mainDiv');
// let newParagraph=document.createElement('p');

// console.log(newParagraph)
//add event listener will take 2 parameters

let div = document.getElementById('mainDiv');
let newParagraph = document.createElement('p');
// console.log(newParagraph);
newParagraph.textContent="Hi, I'm a sample";
console.log(newParagraph.textContent);
// div.appendChild(newParagraph);
div.prepend(newParagraph);
let emptyDiv= document.getElementById('emptyDiv');
emptyDiv.innerHTML='<div><h1>Fruits</h1></div>';

let button = document.getElementById("button");

button.addEventListener('click', function(){
	div.style.backgroundColor='red'
})

div.addEventListener('click', function(){
	div.style.backgroundColor='white'
})