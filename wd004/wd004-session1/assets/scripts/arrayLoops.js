// foreach
// for each element in an array, do this: 
//all for each will have FUNCTION inside 


let fruits= ["apple", "mango", "banana"];

// fruits.forEach (function(fruit, index){
// 	console.log("delicious " +fruit+ " " + index);
// })

/*map
 like for each, but if u save it and save it to a new array 
*/

let fruitSalad = fruits.map(function(fruit){
	return(fruit + "salad")
}) 

console.log(fruitSalad)

let userAge = [15, 7, 21, 74, 12, 22];
//filter

let legalUser = userAge.filter(function(age){
	return (age>17);
})

console.log(legalUser)