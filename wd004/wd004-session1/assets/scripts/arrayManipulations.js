// let fruits = ["apple", "kiwi", "orange"]

// console.log(fruits)

//to remove last element, use (pop)
// will return the removed element

//to add an item at end of the array (push)
// will return the length of the array 

//to remove the first element, use shift()

//to add an item at the start of the array unshift ()
//returns new length of the array

//to add an element to the middle, use splice()
//	it removes and adds at the same time: 


// 	.splice(1st, 2nd, 3rd...)
// 	1st: the index 
// 	2nd: how many elements you want to remove (from the index which we specified in the first argument)
// 	3rd+: elements you want to add from the given index. (optional - will work without this)
// will return the removed object 

