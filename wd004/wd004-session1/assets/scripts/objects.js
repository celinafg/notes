// // an object is a collection of data that comes in 
// // key:value pairs

// let car = {
// 	color: "red",
// 	model: "1",
// 	brand: "e",
// 	addres: "bleh"
// }

// let user = {
// 	name: "john",
// 	lastName: "doe",
// 	isAdmin: false,
// 	address: {
// 		building:{
// 			buildingName: "enzo", 
// 			floor: "3"
// 		},
// 		streetName: "blah",
// 		city: "blah"
// 	},
// 	getName: function () {
// 		return (this.name+" "+this.lastName)
// 		// "this" refers to its current owner which is USER
// 	}
// }


// // function checkAdmin(pikachu){
// // 	if(user.isAdmin===true){
// // 		console.log("You're an Admin");
// // 	} else {
// // 		console.log("You're not an admin")
// // 	}
// // }
// // if we rename the parameter/argument to a name different than the "let" variable, functions will still pass the value into 
// // 	pikachu because in a function, it is just the name of the parameter.


// function checkAdmin(user){
// 	if(user.isAdmin===true){
// 		console.log("You're an Admin");
// 	} else {
// 		console.log("You're not an admin")
// 	}
// }

// // let adminStatus = user.isAdmin;
// checkAdmin(user);


// //add a property
// user.age=25;
// user.isAdmin=false;


// //delete a property
// // delete user ["lastName"]
// console.log(user.getName())