// Array
// type of object that holds multiple sets of values that can be unrelated to each other
// instead of key value pairs, it stores the actual values.
// // accessing value in an array: console.log(array[x])
// let array = [1,2,3,4,5];
// console.log(array[3])
// let num1 = array[3];
// let num2 = array[4];
 
// console.log(num1+num2)

// let fruits = ["apple", "kiwi", "banana"];
// let confusingArray = [1, true, false, "banana"]
// let users = [{
// 	name: "jp",
// 	number: 7
// }, {
// 	name: "dl",
// 	number: 4
// }, {
// 	name: "ma",
// 	number: 2
// }]

// console.log(users[0])
// 	// it can store multipel data
// 	// it can store strings 

