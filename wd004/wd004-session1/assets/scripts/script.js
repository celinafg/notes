// console.log("heelo from script file");

// Javascript Statements 
//Creating a variable
/* 2 ways to create a variable: let, 
	let allows you to redeclare a variable
	not defined = something is worng with ur data*/

// let student = demel;
// console.log(student);
// //not defined  reference error, not a data type

// let student ='demel';
// console.log(student)

// let b56 = "45"
// console.log(b56)

// let myName = "Celina";
// alert (Celina) once you've used the variable, you cannot change or use it again.
//you have to declare a variable to use in a program (variable is like contaners in css)
//Jacascript is a loosely typed language

// let student	= 'demel'
// student = 45
// console.log(student)
// let student= "demel";
// student= "45"
// console.log(student)

/* assignment operators 
*/

// assignment operator 
// let student = 'demel'
//assigning the value to the variable 
// console.log(student)

/*addition assignment operator +=
// adds the variable to the declared */
// let sum =5;
// sum+=2;
// sum+=7;
// sum+=2;
// console.log(sum)

/* Subtraction assignment operator(-=)
let difference = 100;
difference -=15
difference+=15
difference-=50;
console.log(difference)*/

/*multiplication assignment operator 

let product	= 10
product*=3
product*=2
console.log(product)*/

/*quotient division operator assignment 

let quotient = 10
quotient /= 2
quotient /= 2
console.log(quotient)*/

/*modulo operator % remainder 
let mod = 21 % 6
mod%=4
mod%=3
mod%=0
console.log(mod)*/

//arithmetic operators 
/*addition 

let baonMonday=100;
let baonTuesday=300;
let someBaon=baonMonday+baonTuesday

console.log(baonMonday + baonTuesday)
console.log(sum)
subtraction, multiplication, division

let pocketMoney=200
let lunch=100
console.log(pocketMoney-lunch)

let product1 =100
let quantity =10
let customers = 5

let totalPayment = customers*quantity*product1;
console.log(totalPayment /=customers)*/ 

/*increment operator
let sum=10
sum++
sum--
sum --
console.log(sum);*/

/*type coercion: javascript will try to add both and coerce the types. number +"" = contactenate

let student1 = 5
let student2 = "5"

let sum = student1+student2

console.log(sum)

let myName = "500"
alert("hi my name is " - myName )
*/

//equality operator == 
/*console.log('demel'==demel)
//strict compatrison operator ===
console.log(demel=="demel")
console.log(143===143) true*/

//truth vs falsy [0 is false 1 is true]

/*console.log(1==true) //true
console.log(1===true) // false
console.log(1==false) //false
console.log("1"==true)//true
console.log("1"===true) //false
console.log(0===true) //false
console.log(0==true) //false
console.log(0==false) //false
console.log(0===false)//false*/

//strict inequality operator !== 
//lenient inequality operator !=

/*console.log("1" != 1) //false
console.log(1 != false) //true
console.log(154 != false) //true 
console.log(154 !== false) //true
console.log(false !== false) // false */

// ><
//greater than or equal to
//less than or equal to
// console.log(4<1) //false
// console.log(4<"false") //false
// console.log(4>"four") //false 
// console.log(4>"3") //true

// console.log(4>=1) //true
// console.log(4>=4) //true
// console.log(4>=9) //false 
// console.log(4>="4") //true
let variable ="whatever data"