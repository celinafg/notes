<?php require '../templates/template.php'; 
	function get_content(){
?>
<h1 class="text-center py-5">EDIT ARTIST FORM</h1>
<form action="../controller/process_edit_artist.php" method="POST">
	<div class="form-group">
		<label for="name">Name</label>
<?php 
		require '../controller/connection.php';
		$id = $_GET['id'];
		$indiv_artist_query = "SELECT * FROM artists WHERE id = $id";

		$artist = mysqli_query($conn, $indiv_artist_query);
		$updated_artist = mysqli_fetch_assoc($artist);
		
 ?>

		<input type="text" name="name" class="form-control" value="<?php echo $updated_artist['name']?>">
		<input type="hidden" name="id" value="<?php echo $updated_artist['id']?>">
	</div>
	<button type="submit" class="btn btn-success">Update Artist</button>
</form>
<?php
	}
?>