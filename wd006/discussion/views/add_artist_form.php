<?php require '../templates/template.php'; 
	function get_content(){
?>
<h1 class="text-center py-5">ADD ARTIST FORM</h1>
<form action="../controller/process_add_artist.php" method="POST">
	<div class="form-group">
		<label for="name">Name</label>
		<input type="text" name="name" class="form-control">
	</div>
	<button type="submit" class="btn btn-success">Add Artist</button>
</form>
<?php
	}
?>