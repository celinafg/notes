<?php require '../templates/template.php'; 
	function get_content(){
	require '../controller/connection.php'
?>
	<h1 class="text-center py-5">Hello From artists.php</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Artist ID</th>
				<th>Artist Name</th>
				<th>View Details</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$artist_query = "SELECT * FROM artists";
				$artists = mysqli_query($conn, $artist_query);

				foreach ($artists as $indiv_artist){
?>
				<tr>
					<td><?php echo $indiv_artist['id']?></td>
					<td><?php echo $indiv_artist['name']?></td>
					<td><button class="btn btn-info">View Details</button><a href="../controller/process_delete_artist.php?id=<?php echo $indiv_artist['id']?>" class="btn btn-danger">Delete Artist</a>
						<a href="../views/edit_artist_form.php?id=<?php echo $indiv_artist['id']?>" class="btn btn-success">Edit Artist</a></ABBR></td>
				</tr>


<?php
					
				}
			 ?>


		</tbody>
	</table>
<?php		
	}
?>