<html>
<head>
	<meta charset="UTF-8">
	<title>Zodiac Activity</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>


	<form action="zodiacSign.php" method="POST" class="text-center">
		<div class="form-group">
			
			<label for="month">Month</label>
			<input type="number" class= "form-control" name="month">

			<label for="date">Date</label>
			<input type="number" class="form-control" name="date">

		</div>
		<button class="btn btn-primary">Check your Zodiac</button>
	</form>


</body>
</html>