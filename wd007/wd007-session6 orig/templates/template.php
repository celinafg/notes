<html>
<head>
	<meta charset="UTF-8">
	<title>PHP Intro Session</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/solar/bootstrap.css">
</head>
<body>
	
	<?php require "navbar.php" ?>

			<?php get_content(); ?>

	<?php require "footer.php" ?>

	
</body>
</html>