Join vs. subquery 
1. SELECT albums.title 
	FROM albums
	JOIN songs ON songs.album_id = albums.album_id
	WHERE songs.title = 'Kundiman';
	-- this 

2. SELECT title FROM albums WHERE id= 
	(SELECT album_id FROM songs WHERE title = 'Kundiman');
	-- subqueries are more procedural. the thing in the parentheses will find the answer first 
	-- a subquery is a nested query statement 

	SELECT * FROM table1 WHERE column1 = (SELECT column1 from T2);
	SELECT title FROM albums WHERE id = 
	(SELECT album_id FROM songs WHERE title = 'Kundiman');

	RESULT of select statement can be:
	1. SINGLE / scalar value (1 value)
		SELECT title from songs WHERE title = 'Kundiman';
		-- SELECT column name FROM table WHERE column = blah;
		-- SINGLE WHERE STATEMENT 

	2. SINGLE ROW 
		SELECT * FROM songs WHERE title = 'Kundiman';
		-- SINGLE WHERE STATEMENT

	3. SINGLE COLUMN 
		SELECT title FROM songs;
		-- SELECT colname 
		-- NO WHERE CONDITION 

	4. WHOLE TABLE 
		SELECT * FROM songs;
		-- no conditions 

	-- IN WHAT YEAR WAS KISAPMATA RELEASED? 
	SELECT year FROM albums WHERE id = (SELECT album_id FROM songs WHERE title='Kisapmata');
	SELECT name FROM artists WHERE id = (SELECT artist_id FROM albums WHERE year=1996);


	-- WHO SANG GANGNAME STLYE?
	SELECT name FROM artists WHERE id =
	(SELECT artist_id FROM albums WHERE id=
	(SELECT album_id FROM songs WHERE title='Gangnam Style'));

	-- WHO SANG OPM 
	SELECT name FROM artists WHERE id =
	(SELECT artist_id FROM albums WHERE id = 
	(SELECT album_id FROM songs WHERE genre = 'OPM'));

	SELECT name FROM artists WHERE id =
	(SELECT artist_id FROM albums WHERE id=
	(SELECT album_id FROM songs WHERE genre='OPM'));


RETRIEVE THE FOLLOWING DATA USIGNGS SUBQUERIES INSTEAD OF JOIN METHODS. 
-- 1. What is the address of bea alonzos studio? 
SELECT address FROM studios WHERE id = 
(SELECT studio_id FROM artists WHERE screen_name = 'Bea Alonzo');

-- 2. WHat is the title of the movie produced by LAV diaz
SELECT title FROM movies WHERE id =
(SELECT movie_id from movies_producers WHERE producer_id =
(SELECT id FROM producers WHERE name = "Lav Diaz"));

--3. WHO produced Kakabakaba ka ba? 
SELECT name FROM producers WHERE id = 
(SELECT producer_id FROM movies_producers WHERE movie_id =
(SELECT id FROM movies WHERE title = "Kakabakaba ka ba?")); 

-- 4. In what movie did aldren richards star in? 
SELECT title FROM movies WHERE id =
(SELECT movie_id FROM artists_movies WHERE artist_id = 
(SELECT id FROM artists WHERE screen_name = "Alden Richards"));

-- 5. When was the actor who starred in Hello LOve, Goodbye born? 
SELECT birthday FROM artists WHERE id = 
(SELECT artist_id FROM artists_movies WHERE movie_id=
(SELECT id FROM movies = "Hello, Love, Goodbye"));

--6. What is the studio name of the actor who starred in Barumbado? 
SELECT name FROM studios WHERE id =
(SELECT studio_id FROM artists WHERE id =
(SELECT artist_id FROM artists_movies WHERE movie_id = 
(SELECT id FROM movies WHERE title = "Barumbado")));


--To correctly use IN, ANY, ALL operators in the WHERE CLAUSE using mysql
-- more than one value result 

SELECT size, price FROM shirts WHERE id IN 
(SELECT shirt_id FROM materials WHERE material='cotton');
-- WHERE column IN (val, val2...); = WHERE (column=val1) OR (column=val2);
-- WE CAN ONLY USE THIS IF OUR CONDITION IS IN THE SAME COLUMN~!!!!!!!!!!!!!!!!!!!!!
-- USE IN REPLACEMENT OF OR! 


-- back to music store 
INSERT INTO songs (title, length, genre, album_id) VALUES ("Gentleman", 234, "k-pop", 1),
														  ("You Need To Calm Down", 325, "pop", 3), 
														  ("ME!", 273, "pop", 3), 
														  ("Love Story", 345, "pop", 4), 
														  ("You Belong With Me", 234, "pop", 4);

-- What are the album titles of Gentleman, ME! and Love story?
SELECT title FROM albums WHERE id IN
(SELECT album_id FROM songs 
WHERE title IN ("Gentleman", "ME!", "Love Story"));

-- WHO sang OPM, ROCK AND KPOP? 
SELECT name FROM artists WHERE id IN
(SELECT artist_id FROM albums WHERE id IN
(SELECT album_id FROM songs 
WHERE genre IN ("OPM", "rock", "k-pop")));



-- ANY Vs. ALL
-- Both must follow a comparison operator (i.e., =, >, <, ...)
-- both compare a value to a subquery 
-- ANY: result matches ONE of the given values 
-- ALL: result matches all of the given values. 

-- get the prizes and sizes of shirts that are more expensive that 1 of those made from cotton. 

-- get the shirt id 
-- get the price of shirts
--compare all shirt prices to the price of cotton made materials 

SELECT size, price FROM shirts WHERE price > ANY 
(SELECT price FROM shirts WHERE id IN
(SELECT shirt_id FROM materials WHERE material ='cotton'));

CREATE TABLE shirts (
  	id INT NOT NULL AUTO_INCREMENT,
  	size VARCHAR(255), 
  	price INT, 
  	PRIMARY KEY(id)
  	);

CREATE TABLE materials (
  	id INT NOT NULL AUTO_INCREMENT,
  	material VARCHAR(255), 
  	shirt_id INT, 
  	PRIMARY KEY(id)
  	);

INSERT INTO shirts (size, price) VALUES ("S", 450), ("M", 500), ("L", 550), ("XL", 600);

INSERT INTO materials (material, shirt_id) VALUES ("cotton", 3), ("cotton", 1), ("chino", 1), ("denim", 2);

-- activity: get the prizes and sizes of shirts that are
-- 1. more or as expensive than those made from chino
SELECT size, price FROM shirts WHERE price >= ALL 
(SELECT price FROM shirts WHERE id IN
(SELECT shirt_id FROM materials WHERE material ='chino'));

-- 2. less expensive than at least one of the materials made from cotton. 

SELECT size, price FROM shirts WHERE price < ANY 
(SELECT price FROM shirts WHERE id IN
(SELECT shirt_id FROM materials WHERE material ='cotton'));

-- 3. not as expensive than those made from denim 
SELECT size, price FROM shirts WHERE price != ALL 
(SELECT price FROM shirts WHERE id IN
(SELECT shirt_id FROM materials WHERE material ='denim'));

-- what songs titles and length are more than the length of Psy's songs?
SELECT title, length FROM songs WHERE length > ALL
(select length FROM songs WHERE album_id =
(select id FROM albums WHERE artist_id =
(select id FROM artists WHERE name = "Psy")));

--AGGREGATOR OPERATORS 
count (<column>)
SUM ()
MIN()
MAX()
AVERAGE?
GROUP BY 
-- count the values in the column

count (*)
-- count the number of rows 

SELECT COUNT(*) FROM starwars;

SELECT AVG(length) AS avg_length, genre FROM songs 
GROUP BY genre;


SELECT MIN(length) as min_length, title, genre FROM songs;
SELECT MAX(length) as max_length, title, genre FROM songs;

SELECT * FROM songs WHERE length = (SELECT min(length) FROM songs);