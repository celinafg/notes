Advanced SQL selectors 

-- displaying values from a single table 
--SELECT <columns> FROM table;
SELECT title, genre FROM songs; 

--display values of columns from different tables 
--display the name from the albums and title from the songs (at the same time)
-- use the join operator 
-- if there's no relationship between the 2 tables, you can't join them 
--FROM tabl1 JOIN table2 on (join_condition) WHERE <condition>
--name is from ALBUMS; title is from SONGS 
FROM table1 ON table 2 (table1.id = table2_id);
-- table1.id (select the table via its id )
SELECT title, name FROM albums JOIN songs ON (albums.id = songs.album_id);
SELECT genre, length, title FROM albums JOIN songs ON (albums.id = songs.album_id);
--  (albums.id = songs.album_id);

SELECT albums.name AS 'album_name', songs.title AS 'song_title' FROM albums JOIN songs ON (albums.id = songs.album_id);
-- call the table.columnName AS 'new name', songts.title AS 'newtitle' FROM tableAlbums JOIN tableSONGS ON ()

--display the album title and the name of its artist 

-- INSERT INTO artists (name) VALUES ("Taylor Swfit");
SELECT * FROM albums JOIN artists ON (albums.artist_id = artists.id);


-- OUTER JOINS: lets us query the rows with no matches and sets null to fill it in 
-- left join, right join 
-- LEFT JOIN: does not remove rows from the LEFT table, bt only connects rows to the right table 
SELECT * FROM albums LEFT JOIN artists on (albums.artist_id = artists.id);


-- RIGHT JOIN: 
CREATE TABLE A (
	id INT NOT NULL AUTO_INCREMENT, 
	a1 VARCHAR(255), 
	a2 VARCHAR (255), 
	PRIMARY KEY(id)
);

CREATE TABLE B (
	id INT NOT NULL AUTO_INCREMENT, 
	b1 VARCHAR(255), 
	b2 VARCHAR (255), 
	PRIMARY KEY(id)
);


INSERT INTO A (a1, a2) VALUES ("A", 1), 
							  ("B", 3), 
							  ("C", 3), 
							  ("D", 4), 
							  ("E", 4);

INSERT INTO B (b1, b2) VALUES ("V", 1), 
							  ("W", 2), 
							  ("X", 2), 
							  ("Y", 4), 
							  ("Z", 5);

WHAT IS A JOIN B ON (A.id = B.B2) ? 
SELECT * FROM A JOIN B ON (A.id = B.B2);

WHAT IS LEFT JOIN B ON A.id = B.b2? Does not remove rowws from the LEFT table but only oconnects rows from the right. 
SELECT * FROM A LEFT JOIN B on (A.id = B.B2);

WHAT IS LEFT JOIN B on (A.A2 = B.B2);
SELECT * FROM A LEFT JOIN B on (A.A2 = B.B2);
-- LEFT JOIN does not remove rows!!!!!! 
-- RIGHT JOIN is the same as left join except the right will be the one preserved. 

-- LEFT JOIN vs RIGHT JOIN: the side determines which column data will be preserved. if left join, the left column data will be preserved. any cell without a match will return null ONLY on the left table. RIGHT JOIN will preserve the right table cells EVEN IF there is no matching cell in the left table. 

-- go back to activity database from yesterday b46Activity1 and answer the following: 

ADVANCE SELECTS 
LIKE

-- WHERE <column> LIKE <patter> to compare string patterns 
-- use _ and % 

SELECT * FROM songs where title LIKE 'k%';

-- CASE SENSITIVE COMPARISON 
SELECT * FROM songs WHERE title LIKE BINARY 'K%';

-- songs that end with n use % BEFORE what you're looking for. %strings%n
Find the song title that ends with N
SELECT * FROM songs WHERE title LIKE '%n';

-- select form all songs where the title is NOT LIKE N - does not have n at the end
SELECT * FROM songs WHERE title NOT LIKE '%n';


-- select from all songs where the title has a Y in it whether start or end 
SELECT * FROM songs WHERE title LIKE '%Y%';

-- select from all songs where the title has no Y in the entire title 
SELECT * FROM songs WHERE title NOT LIKE '%Y%';

-- select from all songs where the title has either a P or a G in it 
SELECT * FROM songs WHERE title like '%p%' OR title like '%G%'; I 
SELECT * FROM songs WHERE title like '%p%' OR title like 'G%'; I 

SELECT * FROM songs WHERE title LIKE '%p%' OR title LIKE BINARY '%g'

-- does not contain 'p' OR starts with 'g'
-- 
SELECT * FROM songs WHERE title NOT LIKE '%p%' OR title LIKE BINARY 'g%';

-- select the song title that has eight letters 
-- 8 underlines for 8 letters 
SELECT * FROM songs WHERE title LIKE '________';

-- select from songs where 
-- every underscore is 1 character in a string (2 underlines) underscore is strict (includes spaces and special characters)
-- percent sign is either it's at the end or at the beginning. 
SELECT * FROM songs WHERE title LIKE '%t__';

-- activity 
-- find the artist whose name starts with capital T
SELECT * FROM artists WHERE name LIKE BINARY 'T%'; -- Tasylor swift 

-- find the artist whose name doesnt start with 'P';
SELECT * FROM artists WHERE name NOT LIKE 'P%';

-- find the artist whose name ends with y
SELECT * from artists WHERE name LIKE '%y';

-- find the artist whose name contains small letter r
SELECT * from artists WHERE name LIKE BINARY '%r%';

-- find the artist whose name has 3 letters or contains 0
SELECT * FROM artists WHERE name LIKE '___' OR name LIKE '%o%';





-- sorting rows: sort the rows according to the columns given.
-- default is ASC (ascending)
-- DES )descending 

CREATE TABLE starwars (
	id INT NOT NULL AUTO_INCREMENT, 
	title VARCHAR(255),
	year YEAR(4),  
	star VARCHAR (255), 
	PRIMARY KEY(id)
);

INSERT INTO starwars (title, year, star) VALUES ('Star Wars', 1977, 'Carrie Fisher'), 
												('Star Wars', 2015, 'Carrie Fisher'), 
												('Star Wars', 2015, 'Daisy Ridley'), 
												('Star Wars', 1977, 'Harrison Ford'), 
												('Star Wars', 2015, 'Harrison Ford');

-- order by year
-- SELECT * FROM tableName ORDER BY columnName;
SELECT * FROM starwars ORDER BY year;

-- descending 
-- SELECT * FROM tablename ORDER BY columnname DESC;
-- if there are movies published in thesame year (2015), next priority followed is ID no
SELECT * FROM starwars ORDER BY year DESC;

--order by year and star
-- SELECT * FROM tablename ORDER BY columnname1, columnname2;
SELECT * FROM starwars ORDER BY year, star;

-- no duplicates? 
-- SELECT DISTINCT columnname FROM tablename;
SELECT DISTINCT year from starwars;

-- 
SELECT DISTINCT * FROM starwars ORDER BY year;

-- activity: activity 1 ( see notes1,.sql)

--limits in results 
-- SELECT * FROM tablename ORDER BY columnname LIMIT X; 
-- limit 1 for the top etc
SELECT * FROM starwars ORDER BY year LIMIT 1;