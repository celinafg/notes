-- creating a table
-- CREATE TABLE tableName(
--	columnName dataType rulesifany
--	);

--id INT NOT NULL (id should have a value every time)
-- AUTO_INCREMENT (id should increment so we don\t have to do it every time)
-- username VARCHAR(255) NOT NULL (username with a max of 255 char shouold not be non-existent)
-- PRIMARY KEY (id)
--DESCRIBE users (describe the table)
-- SHOW TABLES; (show the tables)
-- CREATE DATABASE databasename;
--SHOW DATABASES;
-- USE databaseName;


CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT, 
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL, 
	PRIMARY KEY (id)
); 

table artists
id 
name 
 -- artists

 CREATE TABLE artists(
 	id INT NOT NULL AUTO_INCREMENT,
 	name VARCHAR(255) NOT NULL,
 	PRIMARY KEY(id)
 );

 --albums
 -- table names are always plural
 -- we specify the primary key as " id", so if we have a foreign key, 
 -- we need to point it to the foreign key (atist_id)
 -- and the source. 
 -- foreign key(key)
 -- 	REFERENCES artists(id) = REFERENCES tableName(columnName)
 -- 	ON UPDATE CASCADE (if we delete an artist ID, everything will follow. if we delete an entry in artist table, all entries referring to the artist deleted will be deleted )
 -- 	ON DELETE RESTRICT (if we delete something from reference table and it has an entry in another table, it will throw an error that you are trying to delete something that was referred. this is very good if we want to make sure that the data will not easily be tampeered with. Ex: you cannot delete an artist if there is an existing album corresponding to artist name. )
 --		ON DELETE SET NULL (if we delete an artist_id, the rows / tables with the artist_id will still exist but the artist-ID (OR WHAT WE ARE TRYING TO DELETE) will be deleted.)		


 CREATE TABLE albums(
 	id INT NOT NULL AUTO_INCREMENT,
 	name VARCHAR(255) NOT NULL, 
 	year YEAR(4),
 	artist_id INT, 
 	PRIMARY KEY(id), 
 	FOREIGN KEY(artist_id)
 		REFERENCES artists(id)
 			ON UPDATE CASCADE
 			ON DELETE CASCADE
 	);

 --songs: title, length, genre, album_id
 -- not null: if you try to save something na walang length mag error siya
 CREATE TABLE songs(
 	id INT NOT NULL AUTO_INCREMENT,
 	title VARCHAR(255) NOT NULL, 
 	length INT, 
 	genre VARCHAR(255),
 	album_id INT, 
 	PRIMARY KEY(id), 
 	FOREIGN KEY(album_id)
 		REFERENCES albums(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL
 	);

 -- playlists: id, date created, user_id

  CREATE TABLE playlists(
  	id INT NOT NULL AUTO_INCREMENT,
  	date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  	user_id INT, 
  	PRIMARY KEY(id), 
  	FOREIGN KEY(user_id)
  		REFERENCES users(id)
  		ON UPDATE CASCADE
  		ON DELETE SET NULL
  	);

  --songs_playlists: id song_id playlist_id
  -- note the comma on the first foreign key after ON DELETE SET NULL. more than 1 foreign key needs comma 

  CREATE TABLE songs_playlists(
  	id INT NOT NULL AUTO_INCREMENT, 
  	song_id INT,
  	playlist_id INT,
  	PRIMARY KEY(id), 
  	FOREIGN KEY(song_id)
  		REFERENCES songs(id)
  		ON UPDATE CASCADE
  		ON DELETE SET NULL,
  	FOREIGN KEY(playlist_id)
  		REFERENCES playlists(id)
  		ON UPDATE CASCADE
  		ON DELETE SET NULL
  	);

  -- errors in naming 
  -- ALTER TABLE tableName RENAME TO artists;
  ALTER TABLE samples RENAME TO edited_samples;

  CREATE TABLE samples (
  	id INT NOT NULL AUTO_INCREMENT,
  	name VARCHAR(255), 
  	age INT, 
  	PRIMARY KEY(id)
  	);
 -- changing column names; 
-- ALTER TABLE tableName CHANGE columnName newName dataTypes
 ALTER TABLE edited_samples CHANGE name sample_name VARCHAR(255);

 -- ADD COLUMN 
-- ALTER TABLE tableName ADD columnName columnDetails;
 ALTER TABLE edited_samples ADD gender VARCHAR(255);

 --DELETE COLUMN
 -- ALTER TABLE tableName DROP columnName;

ALTER TABLE edited_samples DROP age;

-- DELETE TABLE 
-- DROP TABLE tableName;
DROP TABLE edited_samples;

--html, css  javascript, react, angular, vue 

--CRUD OPERATIONS 
-- C: create 
-- R: retrieve 
-- U: update
-- D: delete

--CREATE

INSERT INTO tableName (col1, col2, col3...) VALUES (val1, val2, val3...);
--val1 is the value of col1
-- val2 is the value of col2

-- SELEECT * from tableName (show all contents of tableName)
-- DESCRIBE tableName (shows STRUCTURE of the table)

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (name, year, artist_id) VALUES ("Psy 6", 2012, 2);
INSERT INTO albums (name, year, artist_id) VALUES ("Trip", 1996, 1);

INSERT INTO songs (title, length, genre, album_id) VALUES ("Gangnam Style", 253, "k-pop", 1),
														  ("Kundiman", 234, "OPM", 2),
														  ("Kisapmata", 279, "OPM", 2);
-- SELECT specific column names 
-- SELECT columnName from tableName

-- retrieve with WEHRE condition	
-- SELECT columnName FROM tableName WHERE condition = "";
--SELECT title FROM songs WHERE genre = "OPM";

-- retrieve where genre is KPOP or length = 234
-- SELECT title FROM songs WHERE genre = "kpop" OR length = 234;

-- SELECT columnName FROM tableName WHERE condition = x AND conditio = y;
-- && works 

-- updating entries: ALWAYS INCLUDE A WHERE CONDITION 
-- we need to specify which row we are going to update. failure to do so, it will update verythig. 
-- UPDATE tableName SET col1=newVal1, col2=newVal2 WHERE col3 = val3;
UPDATE songs SET length = 240 WHERE title = "kundiman";

UPDATE songs SET length = 240; -- this makes all songs length to 240
UPDATE songs SET length = 253 WHERE song = "Gangnam style"; -- changing back to original answer
UPDATE songs SET length = 234 WHERE song = "kundiman";

UPDATE songs SET genre = "classic" WHERE genre = "OPM";

-- DELETING A ROW
-- DELETE FROM a tableName WHERE col= value;
DELETE FROM albums WHERE id = 3;
-- DELETING ROWS: can be ANYTHING in the table INCLUDING ID!