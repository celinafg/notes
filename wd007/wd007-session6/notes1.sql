
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT, 
	username VARCHAR(255) NOT NULL, 
	password VARCHAR(255) NOT NULL, 
	PRIMARY KEY(id)
);

CREATE TABLE categories(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255), 
	PRIMARY KEY(id)
);

CREATE TABLE arts(
	id INT NOT NULL AUTO_INCREMENT, 
	description VARCHAR(255), 
	category_id INT, 
	user_id INT, 
	PRIMARY KEY(id), 
	FOREIGN KEY(category_id)
		REFERENCES categories(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL, 
	FOREIGN KEY(user_id)
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE watchlists(
	id INT NOT NULL AUTO_INCREMENT, 
	watcherId INT,
	watcheeId INT,
	PRIMARY KEY(id),
	FOREIGN KEY(watcherId)
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL, 
	FOREIGN KEY(watcheeId) 
		REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

-- activity

CREATE TABLE movies (
 	id INT NOT NULL AUTO_INCREMENT,
 	title VARCHAR(255) NOT NULL, 
 	year YEAR(4), 
 	length INT,
 	genre VARCHAR(255),
 	PRIMARY KEY(id) 
 	);

CREATE TABLE studios (
 	id INT NOT NULL AUTO_INCREMENT,
 	name VARCHAR(255), 
 	address VARCHAR(255), 
  	PRIMARY KEY(id) 
 	);

CREATE TABLE producers (
 	id INT NOT NULL AUTO_INCREMENT,
 	name VARCHAR(255), 
 	address VARCHAR(255), 
  	PRIMARY KEY(id) 
 	);

CREATE TABLE artists (
 	id INT NOT NULL AUTO_INCREMENT,
 	screen_name VARCHAR(255), 
 	address VARCHAR(255), 
 	gender VARCHAR(255),
 	birthday DATE,
 	studio_id INT,
  	PRIMARY KEY(id), 
  	FOREIGN KEY(studio_id)
 		REFERENCES studios(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL
 	);

CREATE TABLE movies_producers (
 	id INT NOT NULL AUTO_INCREMENT,
 	producer_id INT,
 	movie_id INT,
  	PRIMARY KEY(id), 
  	FOREIGN KEY(producer_id)
 		REFERENCES producers(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL, 
 	FOREIGN KEY(movie_id)
 		REFERENCES movies(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL
 	);

CREATE TABLE artists_movies (
 	id INT NOT NULL AUTO_INCREMENT,
 	movie_id INT, 
 	artist_id INT,
  	PRIMARY KEY(id),
  	FOREIGN KEY(movie_id)
 		REFERENCES movies(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL, 
  	FOREIGN KEY(artist_id)
 		REFERENCES artists(id)
 			ON UPDATE CASCADE
 			ON DELETE SET NULL 
 	);


INSERT INTO movies (title, year, length, genre) VALUES ("Hello, Love, Goodbye", 2019, 7020, "romance"),
														("One More Chance", 2000, 8400, "drama"),
														("Kakabakaba ka ba?", 1980, 6240, "thriller"), 
														("Barumbado", 1990, 7456, "action"), 
														("Sa 'Yo Lamang", 2010, 8345, "drama");

INSERT INTO studios (name, address) VALUES ("ABS-CBN", "Eugenio Lopez Dr, Diliman Quezon City, Metro Manila"), 
										   ("GMA","GMA International, 10F GMA Network Center, EDSA cor, Timog Ave, Diliman, Quezon City, 1103 Metro Manila"), 
										   ("TV5", "Reliance St, Mandaluyong, Metro Manila"), 
										   ("RPN", "Broadcast City, 1119 Capitol Hills Dr, Diliman, QuezonCity, Metro Manila");

INSERT INTO producers (name, address) VALUES ("Erik Matti", "Ching Building, 2345 Taft Avenue 1300, Pasay City, Philippines"), 
										 	 ("Joyce Jimenez", "3.N. Fifth Ave. Fort Walton Beach, FL 32547"),
										 	 ("Lily Monteverde", "9970 Newbridge Ave. Blackwood, NJ 08012"),
										 	 ("Lav Diaz", "9325 Rockwell Ave. Memphis, TN 38106");

INSERT INTO artists (screen_name, address, gender, birthday, studio_id) VALUES ("Alden Richards", "121 Thorne Ave. Petersburg, VA 23803", "bicurious", 1992-01-02, 2), 
																				("Bea Alonzo", "7142 E. Greystone Drive Johnson City, TN 37601", "asexual", 1987-10-17, 1), 
																				("Christopher de Leon", "135 Sulphur Springs St. Nazareth, PA 18064", "heterosexual", 1956-10-31, 3), 
																				("Robin Padilla", "94 Cedar Swamp Rd. Seymour, IN 47274", "heterosexual", 1969-11-23, 1);

INSERT INTO movies_producers (producer_id, movie_id) VALUES (1, 1), 
															(1, 2), 
															(2, 1), 
															(3, 4), 
															(3, 4), 
															(4, 5);

INSERT INTO artists_movies (movie_id, artist_id) VALUES (1, 1), 
															(2, 2), 
															(3, 3), 
															(4, 4), 
															(5, 2), 
															(5, 3);															
SELECT title FROM movies WHERE year > 1990;
SELECT screen_name FROM artists WHERE gender = "heterosexual" AND studio_id=1;
SELECT name FROM producers;
UPDATE producers SET address = "510 Shirley St. Logansport, IN 46947." WHERE name = "Erik Matti";
UPDATE artists SET gender = "heterosexual";
DELETE FROM studios WHERE name = "RPN";






-- JOINING TABLES 

1. DISPLAY the screen_name of all artists and his/her corresponding studio name and address. 
- artists: screen_name 
- studios: studio name, address 

SELECT screen_name, name AS studio_name, studios.address AS studio_address FROM artists JOIN studios ON (artists.studio_id = studios.id);
-- FROM always refers to the LEFT table and JOIN to the right. 

2. DISPLAY the movie title and year as well as the name, address and birthday of artists who starred in them .

SELECT title, year, screen_name AS name, birthday 
FROM artists_movies 
JOIN (artists, movies)
ON (artists.id = artists_movies.artist_id AND 
	movies.id = artists_movies.movie_id);


3. DISPLAY all of the producers name as well as the title and genre of the movies they have produced. Display even those that havent produced a movie in the list yet. 

SELECT name, title, genre
FROM movies_producers 
JOIN (movies, producers)
ON (movies.id = movies_producers.movie_id AND 
	producers.id = movies_producers.producer_id);

4. ADD PIOLO PASCUAL IN tbl_producers
INSERT INTO producers (name, address) VALUES ("Piolo Pascual", "Address 5");

5. ADD ANOTHER movie
INSERT INTO movies (title, year, length, genre) VALUES ("Tinimbang Ka Ngunit Kulang", 1974, 8042, "drama");

6. RUN QUERY
SELECT name, title, genre
FROM movies_producers
JOIN (movies, producers)
ON (movies.id = movies_producers.movie_id AND
	producers.id = movies_producers.producer_id);

--selecting name title and genre from the table movies_producers. so we join the movies and producers to show the movie_id from movies column and the producer id 
-- Right join MOVIES 
-- last table mentioned will always be the RIGHTtabe! mentioning another table after the "RIGHT JOIN" will still return the last table AS the right table no matter what. 

SELECT name, title, genre
FROM movies_producers
RIGHT JOIN movies
ON movies.id = movies_producers.movie_id 
RIGHT JOIN producers 
ON producers.id = movies_producers.producer_id;

-- activity 
-- query

1. DISPLAY artists: 
name doesnT END WITH 's'
sort the result by age 

SELECT * FROM artists WHERE screen_name NOT LIKE '%s' ORDER BY birthday DESC; 

2. display name of artists and name of their studios, arrange the result by artists screen_name (z-a);

SELECT screen_name, name AS studio_name 
FROM artists
JOIN studios
ON (artists.studio_id = studios.id)
ORDER BY screen_name DESC;

3. display unique movie titles from artists_movies table. arrannge the result from a-z;

SELECT DISTINCT title 
FROM artists_movies 
JOIN movies
ON (movies.id = artists_movies.movie_id)
ORDER BY title;

4. display producers details and the movie titles, genre  and length ofthe movies they produced; producers name should contain letter 'm', results should be sorted according to the films length 

SELECT name, address, title, genre, length
FROM movies_producers
JOIN (movies, producers)
ON (movies.id = movies_producers.movie_id AND
	producers.id = movies_producers.producer_id)
WHERE name LIKE '%m%'
ORDER BY length;

5. display all details of producers whose name starts with  capital 'P'
and the movies they produced 

SELECT name, address, title
FROM movies_producers 
JOIN (movies, producers)
ON (movies.id = movies_producers.movie_id AND 
	producers.id = movies_producers.producer_id)
WHERE name LIKE BINARY 'P%';

-- 1. Retrieve the screen_name and birthday of artists who are younger than both Bea Alonzo and Christopher de Leon.

SELECT screen_name, birthday FROM artists WHERE birthday > ALL 
(SELECT birthday FROM artists WHERE screen_name IN ('Christopher de Leon', 'Bea Alonzo'));
-- WRONG DATATYPE IN DATABASE


-- 2. In reverse alphabetical order, retrieve the screen_name and studio_id of artists who don’t work for ABS-CBN.

SELECT screen_name, studio_id FROM artists WHERE studio_id IN
(SELECT id FROM studios WHERE name != 'ABS-CBN')
ORDER BY screen_name DESC;
-- 3. Retrieve the screen_name of artists who appeared in Sa ‘Yo Lamang. Only the artist whose name contains a capital letter L should be displayed.

SELECT screen_name FROM artists WHERE id IN
(SELECT artist_id FROM artists_movies WHERE movie_id = 
(SELECT id FROM movies WHERE title = "Sa 'Yo Lamang")) AND screen_name LIKE BINARY '%L%';

-- 4. Retrieve the title and length of movies that  are longer than at least one of the movies produced by Joyce Jimenez and Lav Diaz. Display the result according to the length of the films, from the shortest to the longest.


SELECT title, length FROM movies WHERE length > ANY 
(SELECT length FROM movies WHERE id IN 
(SELECT movie_id FROM movies_producers WHERE producer_id IN 
(SELECT id FROM producers WHERE name IN ('Lav Diaz', 'Joyce Jimenez'))))
ORDER BY length;

-- 5. Retrieve the title and year of drama, romance, or thriller movies that were released after either One More Chance or Barumbado. The result should not include the movies mentioned. (edited) 

SELECT title, year FROM  movies WHERE year > ANY
(SELECT year from movies where title IN ('One More Chance', 'Barumbado'))
AND genre IN ("drama", 'romance', 'thriller') AND title NOT IN ('One More Chance', 'Barumbado');