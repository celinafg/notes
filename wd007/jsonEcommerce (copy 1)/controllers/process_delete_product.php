<?php 
	$name = $_GET['name'];

	$products = file_get_contents("../assets/lib/products.json");

	$products_array = json_decode($products, true);


	//this becomes an associative array: something that php can read as an element 
	//dilemma: it is now an associative array. we want to look through the array to see if the name is = $name.
	//
	foreach($products_array as $index => $product){
		if($name==$product['name']){
			unset($products_array[$index]);
			//unset = "remove" the product with the index given 
		};
	};

	$to_write = fopen("../assets/lib/products.json", 'w');
	fwrite($to_write, json_encode($products_array, JSON_PRETTY_PRINT));

	fclose($to_write);

	header("LOCATION: ".$_SERVER['HTTP_REFERER']);
	//after it's deleted, we want to redirect to the page the function was called. 
	//go back to where you came from 


	//$_POST - info if form is used: not published in url, found in header (?)
	//$_GET - info if URL is used (we used url in catalog.php to delete the fruit): published in the URL
	//$_SERVER - php directory 


 ?>