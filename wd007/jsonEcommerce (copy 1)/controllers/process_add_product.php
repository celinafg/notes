<?php 
	$name = $_POST['name'];
 	$price = intval($_POST['price']);
 	$description = $_POST['description'];
	

 	//we will get the image properties 
 	$filename = $_FILES['image']['name'];
 	$filesize = $_FILES['image']['size'];
 	$file_tmpname = $_FILES['image']['tmp_name'];

 	$file_type = strtolower(
 		pathinfo($filename, PATHINFO_EXTENSION));

 	$hasDetails = false;
 	$isImg = false;

 	if($name != "" && $price > 0 && $description != ""){
 		$hasDetails = true;
 	};

 	if($file_type== "jpg" || $file_type== "jpeg" || $file_type== "png"){
 		$isImg=true;
 	}

 	if($filesize>0 && $isImg== true && $hasDetails ==true){
 		$final_path = "../assets/lib/images/" . $filename;

 		move_uploaded_file($file_tmpname, $final_path);

 	}else{
 		echo "Invalid. TRY AGAIN!";
 	}


 	$image = "images/" . $filename;

 	$newProduct=[
 		"name" => $name,
 		"price" => $price,
 		"description" => $description,
 		"image" => $image
 	];
 
 	$json = file_get_contents("../assets/lib/products.json");

 	$products =json_decode($json, true);

 	array_push($products, $newProduct);

 	//write new array to products.json file
 	//w is write (write )
 	$to_write = fopen("../assets/lib/products.json", "w");

 	//json pretty print- for real
 	//follows the format 
 	fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));
 	
 	fclose($to_write);

 	header("Location: ../views/catalog.php");
 	//go back to location 

 ?>

 <!-- 
 whenever we save a file, it wil be saved to a global variable $FILES
 tempname = where the file is (so we can move it to our directory)
 file type = to check if the user is uploading an image (jpeg, pdf etc)
 	 strtolower = transforms into lowercase
 	 pathinfo(filename, EXTENSION) = print out what extension was uploaded - jpeg etc
 file size = if 0, no photo uploaded
 contcatenate: .
 $hasDetails = 

 "sanitize" make sure all input is in the same format that we want 

	* variable from the form = $_POST['name']
		we are getting the data from our form from the POST file and selecting the info we want which is 'NAME'
	$name = $_POST['name'];
 	$price = $_POST['price'];
 	$description = $_POST['description'];
	

 	* variablename = $_FILES['image']['size']
 		assign a variable name = retrieve image + size from the $_FILES directory
 		assign a variable name for size = retrieve image + size from the $_FILES directory
 		temp name is the name of uploaded file = retrieve the image and temp name from the $_FILES directory
 	$filename = $_FILES['image']['name'];
 	$filesize = $_FILES['image']['size'];
 	$file_tmpname = $_FILES['image']['tmp_name'];


	* variablename = convert to lowercase the (filename and path extension)
 	$file_type = strtolower(
 		pathinfo($filename, PATHINFO_EXTENSION));


	* this part validates if there was something uploaded; $hasDetails and $isImg should initially be FALSE 
 	$hasDetails = false;
 	$isImg = false;

 	if($name != "" && $price > 0 && $description != ""){
 		$hasDetails = true;
 	};

 	if($file_type== "jpg" || $file_type== "jpeg" || $file_type== "png"){
 		$isImg=true;
 	}

 	if($filesize>0 && $isImg== true && $hasDetails ==true){
 		$final_path = "../assets/lib/images/" . $filename;

 		move_uploaded_file($file_tmpname, $final_path);
 		
 	}else{
 		echo "Invalid. TRY AGAIN!";
 	}


 	$image = "images/" . $filename;

 	$newProduct=[
 		"name" => $name,
 		"price" => $price,
 		"description" => $description,
 		"image" => $image
 	];
 
 	$json = file_get_contents("../assets/lib/products.json");

 	$products =json_decode($json, true);

 	array_push($products, $newProduct);

 	//write new array to products.json file
 	//w is write (write )
 	$to_write = fopen("../assets/lib/products.json", "w");

 	//json pretty print- for real
 	//follows the format 
 	fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));
 	
 	fclose($to_write);

 	header("Location: ../views/catalog.php");
 	//go back to location 

 ?>












 -->