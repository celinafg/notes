<?php 
	session_start();

	//this function will allow us to use $_SESSION . it's required if we want to use $_SESSION
	$name = $_POST['name'];
	$quantity = $_POST['quantity'];
	
	
	// $_SESSION[$name]
	/* 1st time adding to cart: we need to create $_session on the first time we order. 
	create session cart -> keyName product name 
	$_SESSION['cart'][$name] = $quantity. */

	//we need to know if there is an existing session already 
	//isset is a function that returns true if the variable inside exists
	//the reverse: first time; 

	if(!isset($_SESSION['cart'][$name])){
		$_SESSION['cart'][$name] = $quantity;
	} else {
		$_SESSION['cart'][$name] += $quantity;
	}

	header("LOCATION: ../views/cart.php");





	// 3/4th time adding to cart of the same product:
 ?>