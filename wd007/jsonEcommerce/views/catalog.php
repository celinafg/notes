<?php 
	require "../templates/template.php";

	function get_content(){
?>
	<h1 class="text-center py-4">CATALOG</h1>
	<div class="container">
		<div class="row">
			<?php 
			$products = file_get_contents("../assets/lib/products.json");
			// var_dump($products);
			//die();
			
			$products_array = json_decode(
				$products, true);
			// var_dump($products_array);
			// die();

			foreach($products_array as $indiv_product){
			?>
			<div class="col-lg-4 py-2">
				<div class="card">
					<img class="card-img-top" height="300px" src="../assets/lib/<?php echo $indiv_product['image'] ?>">
					<div class="card-body">
						<h5 class="card-title"><?php echo $indiv_product['name'] ?></h5>
						<p class="card-text">Price: <?php echo $indiv_product['price'] ?></p>
						<p class="card-text">Description: <?php echo $indiv_product['description'] ?></p>
					</div>
					<?php 
						if(isset($_SESSION['email']) && $_SESSION['email']=="admin@admin.com"){
?>
						<div class="card-footer">
						<a href="../controllers/process_delete_product.php?name=<?php echo $indiv_product['name']?>" class="btn btn-danger">DELETE Fruit</a>
						<!-- we are deleting items by pointing the process_delete file and deleting via the PRODUCT NAME (?name) this is the equivalent of the "name" in input  -->
					</div>
					<?php
						} else {
?>
					<div class="card-footer">
					<form action="../controllers/process_addToCart.php" method="POST">
						<input name="name" type="hidden" value="<?php echo $indiv_product['name']?>">
						<!-- technique used to assign a default value without the user seeing  -->
						<input type="number" name="quantity" value="1" class="form-control">
						<button type="submit" class="btn btn-info">Add to cart</button>
					</form>
					</div>

<?php
						}
						?>
					
					
				</div>
			</div>		
			
			<?php	
			}

			 ?>
		</div>
	</div>
<?php	
	}
 ?>

 <!-- 
 file_get_contents: gets contents of a specific file, here it is products.json
 json_decode: true = makes it an associative array
 curly brace, ?> 
 associative array? 
 multi dimensional 
 array within an aray (get arrays as individual elements)


 /*
slash is not recognized in php. needs a backslash in addition to the forward slash 
MVC (model, view, controller) 
- a way to set up our website 
- view: takes care of User Interface (file a user can access)
- model: takes care of our schema (?)
- controller: file the user will process -->