<?php 
	require "../templates/template.php";

	function get_content() {
?>
	<h1 class="text-center py-4">Cart Page</h1>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<th>Fruit Name:</th>
						<th>Price:</th>
						<th>Quantity:</th>
						<th>Subtotal:</th>
					</thead>
					<tbody>
						<!-- goals 
							1. access session cart
							2. access json file for product specs-->

						<?php 
						//review this part later 
						// session_start();


						$products = file_get_contents("../assets/lib/products.json");
						//we have accessed the json file 
						$products_array = json_decode($products, true);
						//we are decoding the json file to be PHP elements 

						$total = 0;
						// 1. check if session exists 
						if(isset($_SESSION['cart'])) {
							//if session['cart'] exists, loop through the array getting the name and the quantity
							foreach($_SESSION['cart'] as $name => $quantity){
							//loop through our $products_array to get the individual product information 
								foreach ($products_array as $indiv_product) {
								//check if $name (from $_session['cart'] is equal to $indiv_product['name'])
				
									if($name == $indiv_product['name']){
										$subtotal= $quantity*$indiv_product['price'];
										$total += $subtotal;
										?>

										<tr>
											<td><?php echo $indiv_product['name'] ?></td>
											<td>PHP<?php echo $indiv_product['price'] ?></td>
											<td><?php echo $quantity ?></td>
											<td>PHP<?php echo $subtotal ?></td>
											<td>
										 		<a href="../controllers/process_removeitem.php?name=<?php echo $indiv_product['name']?>" class="btn btn-danger">Remove Item</a>
										 	</td>
										</tr>

										<?php
										// var_dump($subtotal);
										// die();
									}
									
								}
							}
						} 

						 ?>

						 <tr>
						 	<td></td>
						 	<td></td>
						 	<td><a href="../controllers/process_emptycart.php" class="btn btn-danger">Empty Cart</a></td>
						 	<td>Total:PHP <?php echo $total ?>.00</td>
						 	
						 </tr>
					</tbody>
				</table>
			</div>			
		</div>
	</div>
<?php
	}

 ?>